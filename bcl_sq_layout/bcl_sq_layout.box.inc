<?php

/**
 * @file
 * bcl_sq_layout.box.inc
 */

/**
 * Implements hook_default_box().
 */
function bcl_sq_layout_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'headerbox';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Displays the title and slogan at the top of the front page.';
  $box->options = array(
    'additional_classes' => '',
    'body' => array(
      'format' => 'full_html',
      'value' => '<h1>Përkthimi i Programeve - E Duam Kompjuterin Shqip</h1>',
    ),
  );
  $export['headerbox'] = $box;

  return $export;
}
