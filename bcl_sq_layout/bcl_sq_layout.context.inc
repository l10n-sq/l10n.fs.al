<?php

/**
 * @file
 * bcl_sq_layout.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function bcl_sq_layout_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'admin';
  $context->description = 'Display development block to admin.';
  $context->tag = '';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'administrator' => 'administrator',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-devel' => array(
          'delta' => 'devel',
          'module' => 'menu',
          'region' => 'footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Display development block to admin.');
  $export['admin'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'content';
  $context->description = 'Pages, blogs, books, etc.';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'options' => array(
        'node_form' => '0',
      ),
      'values' => array(
        'article' => 'article',
        'blog' => 'blog',
        'book' => 'book',
        'page' => 'page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'disqus-disqus_comments' => array(
          'delta' => 'disqus_comments',
          'module' => 'disqus',
          'region' => 'content',
          'weight' => '-8',
        ),
        'disqus-disqus_popular_threads' => array(
          'delta' => 'disqus_popular_threads',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'disqus-disqus_recent_comments' => array(
          'delta' => 'disqus_recent_comments',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'disqus-disqus_top_commenters' => array(
          'delta' => 'disqus_top_commenters',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'system-main' => array(
          'delta' => 'main',
          'module' => 'system',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Pages, blogs, books, etc.');
  $export['content'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'frontpage';
  $context->description = 'Layout of the front page.';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-headerbox' => array(
          'delta' => 'headerbox',
          'module' => 'boxes',
          'region' => 'header',
          'weight' => '-9',
        ),
        'btrTranslations-statistics' => array(
          'delta' => 'statistics',
          'module' => 'btrTranslations',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'btrTranslations-topcontrib' => array(
          'delta' => 'topcontrib',
          'module' => 'btrTranslations',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'system-main' => array(
          'delta' => 'main',
          'module' => 'system',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Layout of the front page.');
  $export['frontpage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'page';
  $context->description = 'Layout of a page.';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about' => 'about',
        'contact' => 'contact',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'aggregator-feed-1' => array(
          'delta' => 'feed-1',
          'module' => 'aggregator',
          'region' => 'sidebar_first',
          'weight' => '-24',
        ),
        'btrTranslations-statistics' => array(
          'delta' => 'statistics',
          'module' => 'btrTranslations',
          'region' => 'sidebar_first',
          'weight' => '-22',
        ),
        'btrTranslations-topcontrib' => array(
          'delta' => 'topcontrib',
          'module' => 'btrTranslations',
          'region' => 'sidebar_first',
          'weight' => '-21',
        ),
        'disqus-disqus_comments' => array(
          'delta' => 'disqus_comments',
          'module' => 'disqus',
          'region' => 'content',
          'weight' => '-8',
        ),
        'disqus-disqus_popular_threads' => array(
          'delta' => 'disqus_popular_threads',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'disqus-disqus_recent_comments' => array(
          'delta' => 'disqus_recent_comments',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'disqus-disqus_top_commenters' => array(
          'delta' => 'disqus_top_commenters',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'system-main' => array(
          'delta' => 'main',
          'module' => 'system',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Layout of a page.');
  $export['page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-help' => array(
          'delta' => 'help',
          'module' => 'system',
          'region' => 'help',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['sitewide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'translations';
  $context->description = 'The pages where the translation/feedback activity is happening (module btrClient).';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'translations/*' => 'translations/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'disqus-disqus_popular_threads' => array(
          'delta' => 'disqus_popular_threads',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'disqus-disqus_recent_comments' => array(
          'delta' => 'disqus_recent_comments',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'disqus-disqus_top_commenters' => array(
          'delta' => 'disqus_top_commenters',
          'module' => 'disqus',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'system-main' => array(
          'delta' => 'main',
          'module' => 'system',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('The pages where the translation/feedback activity is happening (module btrClient).');
  $export['translations'] = $context;

  return $export;
}
